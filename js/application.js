
        // Define a new module for our app. The array holds the names of dependencies if any.
        var app = angular.module("appController", ['$scope','$http']);
        window.timer = new Object();
        
        // The controller

        function appController($scope,$http){

            // The data model. These items would normally be requested via AJAX,
            // but are hardcoded here for simplicity. See the next example for
            // tips on using AJAX.

            $scope.exchanges = [
                {
                    url: 'http://mtgox.com/',
                    title: 'Mt.Gox',
                    name: 'mtgx',
                    logo: 'img/mtgx.png',
                    urls: {
                        btc: {
                            usd: 'https://data.mtgox.com/api/1/BTCUSD/ticker'
                        }
                    },
                    buy: false,
                    sell: false,
                    include: false

                },
                {
                    url: 'http://bitstamp.com',
                    title: 'Bitstamp',
                    name: 'btsp',
                    logo: 'img/btsp.png',
                    urls: {
                        btc: {
                            usd: 'https://www.bitstamp.net/api/ticker/'
                        }
                    },
                    buy:false,
                    sell:false,
                    include: false
                },
                {
                    url: 'http://btc-e.com',
                    title: 'BTC-e',
                    name: 'btce',
                    logo: 'img/btce.png',
                    urls: {
                        btc: {
                            usd: 'https://btc-e.com/api/2/btc_usd/ticker'
                        },
                        ltc: {
                            btc: 'https://btc-e.com/api/2/ltc_btc/ticker',
                            usd: 'https://btc-e.com/api/2/ltc_usd/ticker'
                        },
                        nmc: {
                            btc: 'https://btc-e.com/api/2/nmc_btc/ticker',
                            usd: 'https://btc-e.com/api/2/nmc_usd/ticker'
                        },
                        ppc: {
                            btc: 'https://btc-e.com/api/2/ppc_btc/ticker',
                            usd: 'https://btc-e.com/api/2/ppc_usd/ticker'
                        }
                    },
                    buy: false,
                    sell: false,
                    include: false
                },
                {
                    url: 'https://bter.com/ref/10892',
                    title: 'BTer',
                    name: 'bter',
                    logo: 'img/bter.png',
                    urls: {
                        btc: {
                            cny: 'https://bter.com/api/1/ticker/btc_cny',
                            usd: true
                        },
                        ltc: {
                            btc: 'https://bter.com/api/1/ticker/ltc_btc',
                            cny: 'https://bter.com/api/1/ticker/ltc_cny',
                            usd: true
                        },
                        nmc: {
                            btc: 'https://bter.com/api/1/ticker/nmc_btc',
                            ltc: 'https://bter.com/api/1/ticker/nmc_ltc',
                            cny: 'https://bter.com/api/1/ticker/nmc_cny'
                        },
                        ppc: {
                            btc: 'https://bter.com/api/1/ticker/ppc_btc',
                            ltc: 'https://bter.com/api/1/ticker/ppc_ltc',
                            cny: 'https://bter.com/api/1/ticker/ppc_cny'
                        },
                        qrk: {
                            btc: 'https://bter.com/api/1/ticker/qrk_btc'
                        }
                    },
                    buy: false,
                    sell: false,
                    include: false

                },{
                    url: 'http://therocktrading.com/',
                    title: 'Rock Trade',
                    name: 'rock',
                    logo: 'img/rock.jpg',
                    urls: {
                        btc: {
                            usd: 'https://www.therocktrading.com/api/ticker/BTCUSD',
                            eur: 'https://www.therocktrading.com/api/ticker/BTCEUR',
                            btc: 'https://www.therocktrading.com/api/ticker/BTCUSD',
                            xrp: 'https://www.therocktrading.com/api/ticker/BTCXRP',
                        },
                        ltc: {
                            btc: 'https://www.therocktrading.com/api/ticker/LTCBTC',
                            eur: 'https://www.therocktrading.com/api/ticker/LTCEUR',
                            usd: 'https://www.therocktrading.com/api/ticker/LTCUSD'
                        }
                    },
                    buy: false,
                    sell: false,
                    include: false

                },
                {
                    url: 'http://bitfinex.com/',
                    title: 'Bitfinex',
                    name: 'bfnx',
                    logo: 'img/bfnx.png',
                    urls: {
                        btc: {
                            usd: 'https://api.bitfinex.com/v1/ticker/btcusd',
                        },
                        ltc: {
                            btc: 'https://api.bitfinex.com/v1/ticker/ltcbtc',
                            usd: 'https://api.bitfinex.com/v1/ticker/ltcusd'
                        }
                    },
                    buy: false,
                    sell: false,
                    include: false

                },
                {
                    url: 'http://kraken.com/',
                    title: 'Kraken',
                    name: 'krkn',
                    logo: 'img/kraken.png',
                    urls: {
                        ltc: {
                            usd: 'https://api.kraken.com/0/public/Ticker?pair=ltcusd',
                        },
                        nmc: {
                            usd: 'https://api.kraken.com/0/public/Ticker?pair=nmcusd'
                        }
                    },
                    buy: false,
                    sell: false,
                    include: false

                },{
                    url: 'https://crypto-trade.com/ref/joesmoe',
                    title: 'Crypto-Trade',
                    name: 'cryptotrade',
                    logo: 'img/cryptotrade.png',
                    urls: {
                        btc: {
                            usd: 'https://crypto-trade.com/api/1/ticker/btc_usd',
                        },
                        ltc: {
                            usd: 'https://crypto-trade.com/api/1/ticker/ltc_usd',
                            btc: 'https://crypto-trade.com/api/1/ticker/ltc_btc'
                        },
                        nmc: {
                            usd: 'https://crypto-trade.com/api/1/ticker/nmc_usd',
                            btc: 'https://crypto-trade.com/api/1/ticker/nmc_btc'
                        },
                        ppc: {
                            usd: 'https://crypto-trade.com/api/1/ticker/ppc_usd'
                        }
                    },
                    buy: false,
                    sell: false,
                    include: false

                },{
                    url: 'http://btcchina.com/',
                    title: 'BtcChina',
                    name: 'btcchina',
                    logo: 'img/btcchina.png',
                    urls: {
                        btc: {
                            //usd: 'https://crypto-trade.com/api/1/ticker/btc_usd',
                            cny: 'https://data.btcchina.com/data/ticker',
                            usd: true
                        }
                        
                    },
                    buy: false,
                    sell: false,
                    include: false

                },{
                    url: 'http://www.okcoin.com/?invite=3251343',
                    title: 'OkCoin',
                    name: 'okcoin',
                    logo: 'img/okcoin.png',
                    urls: {
                        btc: {
                            cny: 'https://www.okcoin.com/api/ticker.do',
                            usd: true
                        }
                    },
                    buy: false,
                    sell: false,
                    include: false

                },{
                    url: 'https://CampBX.com/register.php?r=h0Qn9H4H9wF',
                    title: 'CampBX',
                    name: 'campbx',
                    logo: 'img/campbx.png',
                    urls: {
                        btc: {
                            usd: 'http://campbx.com/api/xticker.php'
                        }
                    },
                    buy: false,
                    sell: false,
                    include: false

                },

                {
                    url: 'https://www.coinbase.com',
                    title: 'CoinBase',
                    name: 'cbase',
                    logo: 'img/cbase.jpg',
                    urls: {
                        btc: {
                            usd: 'https://coinbase.com/api/v1/prices/'
                        }
                    },
                    buy: false,
                    sell: false,
                    include: false

                },

                {
                    url: 'https://vircurex.com/welcome/index?referral_id=806-9148',
                    title: 'Vircurex',
                    name: 'vircurex',
                    logo: 'img/vircurex.jpg',
                    urls: {
                        btc: {
                            usd: 'https://vircurex.com/api/get_info_for_1_currency.json?base=BTC&alt=USD',
                        },
                        ltc: {
                            btc: 'https://vircurex.com/api/get_info_for_1_currency.json?base=LTC&alt=BTC',
                            usd: 'https://vircurex.com/api/get_info_for_1_currency.json?base=LTC&alt=USD',
                        },
                        nmc: {
                            btc: 'https://vircurex.com/api/get_info_for_1_currency.json?base=NMC&alt=BTC',
                            ltc: 'https://vircurex.com/api/get_info_for_1_currency.json?base=NMC&alt=LTC',
                        },
                        ppc: {
                            btc: 'https://vircurex.com/api/get_info_for_1_currency.json?base=PPC&alt=BTC',
                            ltc: 'https://vircurex.com/api/get_info_for_1_currency.json?base=PPC&alt=LTC',
                        },
                    },
                    buy: false,
                    sell: false,
                    include: false

                }
                // FUCKED UP API
                //,{
                //     url: 'http://cryptsy.com/',
                //     title: 'Cryptsy',
                //     name: 'cryptsy',
                //     logo: 'img/cryptsy.png',
                //     urls: {
                //         ltc: {
                //             btc: 'http://pubapi.cryptsy.com/api.php?method=singleorderdata&marketid=3',
                //         },
                //         nmc: {
                //             btc: 'http://pubapi.cryptsy.com/api.php?method=singleorderdata&marketid=29',
                //         },
                //         ppc: {
                //             btc: 'http://pubapi.cryptsy.com/api.php?method=singleorderdata&marketid=28',
                //         }
                //     },
                //     buy: false,
                //     sell: false,
                //     include: false

                // },

                
            ];

            $scope.currencies = [
                {
                    title: 'BTC',
                    name: 'btc',
                    logo: 'img/bitcoin.jpg'
                },{
                    title: 'LTC',
                    name: 'ltc',
                    logo: 'img/ltc.jpg'
                },{
                    title: 'NMC',
                    name: 'nmc',
                    logo: 'img/nmc.png',
                },{
                    title: 'PPC',
                    name: 'ppc',
                    logo: 'img/ppc.png',
                },{
                    title: 'QRK',
                    name: 'qrk',
                    logo: 'img/qrk.png',
                },                {
                    title: 'USD',
                    name: 'usd',
                    logo: 'img/usd.png',
                },{
                    title: 'CNY',
                    name: 'cny',
                    logo: 'img/cny.png',
                }

            ];

            $scope.first = false;
            $scope.base = false;
            $scope.exchange = [];

            $scope.currencyFilter = function (items) {
                var result = {};
                angular.forEach(items, function(value, key) {
                    if (value.urls.hasOwnProperty($scope.first) && value.urls[$scope.first].hasOwnProperty($scope.base)) {
                        result[key] = value;
                    } else if(value.urls.hasOwnProperty($scope.base) && value.urls[$scope.base].hasOwnProperty($scope.first)) {
                        result[key] = value;
                    }
                });
                return result;
            };

            $scope.tableFilter = function (items) {
                var result = {};
                angular.forEach(items, function(value, key) {
                    if (value.urls.hasOwnProperty($scope.first) && value.urls[$scope.first].hasOwnProperty($scope.base) && value.buy>0) {
                        result[key] = value;
                    } else if (value.urls.hasOwnProperty($scope.base) && value.urls[$scope.base].hasOwnProperty($scope.first) && value.buy>0) {
                        result[key] = value;
                    }
                });
                return result;
            };

            $scope.select = function (id) {
                if($scope.first && !$scope.base) {
                    $scope.base = id;
                    var revert = true;
                    angular.forEach($scope.exchanges, function(value, key) {
                        if(value.urls.hasOwnProperty($scope.first) && value.urls[$scope.first].hasOwnProperty($scope.base))
                        {
                            revert = false;
                        }
                    });
                    if(revert == true) {
                        $scope.base = $scope.first;
                        $scope.first = id;
                    }
                } else {
                    $scope.first = id;
                    $scope.base = false;
                    angular.forEach($scope.exchanges, function(value, key) {
                        value.buy=0;
                        window.clearInterval(window.timer[key]);
                        window.timer[key] = false;
                    });
                }
                //$scope.$apply();
            };

            $scope.selectLast = function (id) {
                
                var getRates = function(id,exchanges,first,base) {
                    var e;
                    var id=id;
                    var first=first;
                    var base=base;
                    //execute api calls to get prices
                    for (var i in $scope.exchanges)
                    {
                        if($scope.exchanges[i].name == id)
                        {
                            e = exchanges[i];
                            //check if already set
                            if(window.timer[i]) {
                                e.buy = false;
                                window.clearInterval(window.timer[i]);
                                window.timer[i] = false;
                                return;
                            }
                            e.buy = "0";
                            switch (id) {
                                    case "mtgx":
                                        var url = e.urls[first][base];
                                        $http.get("?url="+encodeURIComponent(url)).success(function(data){
                                            e.buy = data['return']['sell']['value'];
                                            e.sell = data['return']['buy']['value'];
                                            e.include = true;
                                        });
                                        break;
                                    case "btce":
                                        var url = e.urls[first][base];
                                        $http.get("?url="+encodeURIComponent(url)).success(function(data){
                                            e.buy = data.ticker.buy;
                                            e.sell = data.ticker.sell;
                                            e.include = true;
                                        });
                                        break;
                                    case "btsp":
                                        var url = e.urls[first][base];
                                        $http.get("?url="+encodeURIComponent(url)).success(function(data){
                                            e.buy = data.ask;
                                            e.sell = data.bid;
                                            e.include = true;
                                        });
                                        break;
                                    case "bter":
                                        if(base == "usd")
                                        {
                                            $http.get("?url="+encodeURIComponent("http://rate-exchange.appspot.com/currency?from=CNY&to=USD")).success(function(data){
                                                exchangeMultiplier = data.rate;
                                                var url = e.urls[first]['cny'];
                                                $http.get("?url="+encodeURIComponent(url)).success(function(data){
                                                    e.buy = exchangeMultiplier*data.sell;
                                                    e.sell = exchangeMultiplier*data.buy;
                                                    e.include = true;
                                                });
                                            }); 
                                        } else {
                                            var url = e.urls[first][base];
                                            $http.get("?url="+encodeURIComponent(url)).success(function(data){
                                                e.buy = data.sell;
                                                e.sell = data.buy;
                                                e.include = true;
                                            });
                                        }
                                        break;
                                    case "rock":
                                        var url = e.urls[first][base];
                                        $http.get("?url="+encodeURIComponent(url)).success(function(data){
                                            e.buy = data.result[0].ask;
                                            e.sell = data.result[0].bid;
                                            e.include = true;
                                        });
                                        break;
                                    case "bfnx":
                                        var url = e.urls[first][base];
                                        $http.get("?url="+encodeURIComponent(url)).success(function(data){
                                            e.buy = data.ask;
                                            e.sell = data.bid;
                                            e.include = true;
                                        });
                                        break;
                                    case "krkn":
                                        var url = e.urls[first][base];
                                        $http.get("?url="+encodeURIComponent(url)).success(function(data){
                                            e.buy = data.result["X"+first.toUpperCase()+"Z"+base.toUpperCase()].a[0];
                                            e.sell = data.result["X"+first.toUpperCase()+"Z"+base.toUpperCase()].b[0];
                                            e.include = true;
                                        });
                                        break;
                                    case "cryptotrade":
                                        var url = e.urls[first][base];
                                        $http.get("?url="+encodeURIComponent(url)).success(function(data){
                                            e.buy = data.data.min_ask;
                                            e.sell = data.data.max_bid;
                                            e.include = true;
                                        });
                                        break;
                                    case "vircurex":
                                        var url = e.urls[first][base];
                                        $http.get("?url="+encodeURIComponent(url)).success(function(data){
                                            e.buy = data.lowest_ask;
                                            e.sell = data.highest_bid;
                                            e.include = true;
                                        });
                                        break;
                                    case "campbx":
                                        var url = e.urls[first][base];
                                        $http.get("?url="+encodeURIComponent(url)).success(function(data){
                                            e.buy = data['Best Ask'];
                                            e.sell = data['Best Bid'];
                                            e.include = true;
                                        });
                                        break;
                                    case "cbase":
                                        var url = e.urls[first][base];
                                        $http.get("?url="+encodeURIComponent(url+'buy')).success(function(data){
                                            e.buy = data.total.amount;
                                            $http.get("?url="+encodeURIComponent(url+'sell')).success(function(data){
                                                e.sell = data.total.amount;
                                                e.include = true;
                                            });
                                        });
                                        break;
                                    case "btcchina":
                                        if(base == "usd")
                                        {
                                            $http.get("?url="+encodeURIComponent("http://rate-exchange.appspot.com/currency?from=CNY&to=USD")).success(function(data){
                                                exchangeMultiplier = data.rate;
                                                var url = e.urls[first]['cny'];
                                                $http.get("?url="+encodeURIComponent(url)).success(function(data){
                                                    e.buy = exchangeMultiplier*data.ticker.sell;
                                                    e.sell = exchangeMultiplier*data.ticker.buy;
                                                    e.include = true;
                                                });
                                            }); 
                                        } else {
                                            var url = e.urls[first]['cny'];
                                            $http.get("?url="+encodeURIComponent(url)).success(function(data){
                                                e.buy = data.ticker.sell;
                                                e.sell = data.ticker.buy;
                                                e.include = true;
                                            });
                                        }
                                        break;
                                    case "okcoin":
                                        if(base == "usd")
                                        {
                                            $http.get("?url="+encodeURIComponent("http://rate-exchange.appspot.com/currency?from=CNY&to=USD")).success(function(data){
                                                exchangeMultiplier = data.rate;
                                                var url = e.urls[first]['cny'];
                                                $http.get("?url="+encodeURIComponent(url)).success(function(data){
                                                    e.buy = exchangeMultiplier*data.ticker.sell;
                                                    e.sell = exchangeMultiplier*data.ticker.buy;
                                                    e.include = true;
                                                });
                                            }); 
                                        } else {
                                            var url = e.urls[first]['cny'];
                                            $http.get("?url="+encodeURIComponent(url)).success(function(data){
                                                e.buy = data.ticker.sell;
                                                e.sell = data.ticker.buy;
                                                e.include = true;
                                            });
                                        }
                                        break;

                                }
                            window.timer[i] = window.setInterval(function(){
                                var version = Math.random();
                                switch (id) {
                                    case "mtgx":
                                        var url = e.urls[first][base];
                                        $http.get("?v="+version+"&url="+encodeURIComponent(url)).success(function(data){
                                            e.buy = data['return']['sell']['value'];
                                            e.sell = data['return']['buy']['value'];
                                            e.include = true;
                                        });
                                        break;
                                    case "btce":
                                        var url = e.urls[first][base];
                                        $http.get("?v="+version+"&url="+encodeURIComponent(url)).success(function(data){
                                            e.buy = data.ticker.buy;
                                            e.sell = data.ticker.sell;
                                            e.include = true;
                                        });
                                        break;
                                    case "btsp":
                                        var url = e.urls[first][base];
                                        $http.get("?v="+version+"&url="+encodeURIComponent(url)).success(function(data){
                                            e.buy = data.ask;
                                            e.sell = data.bid;
                                            e.include = true;
                                        });
                                        break;
                                    case "bter":
                                        if(base == "usd")
                                        {
                                            $http.get("?v="+version+"&url="+encodeURIComponent("http://rate-exchange.appspot.com/currency?from=CNY&to=USD")).success(function(data){
                                                exchangeMultiplier = data.rate;
                                                var url = e.urls[first]['cny'];
                                                $http.get("?v="+version+"&url="+encodeURIComponent(url)).success(function(data){
                                                    e.buy = exchangeMultiplier*data.sell;
                                                    e.sell = exchangeMultiplier*data.buy;
                                                    e.include = true;
                                                });
                                            }); 
                                        } else {
                                            var url = e.urls[first][base];
                                            $http.get("?v="+version+"&url="+encodeURIComponent(url)).success(function(data){
                                                e.buy = data.sell;
                                                e.sell = data.buy;
                                                e.include = true;
                                            });
                                        }
                                        break;
                                    case "rock":
                                        var url = e.urls[first][base];
                                        $http.get("?v="+version+"&url="+encodeURIComponent(url)).success(function(data){
                                            e.buy = data.result[0].ask;
                                            e.sell = data.result[0].bid;
                                            e.include = true;
                                        });
                                        break;
                                    case "bfnx":
                                        var url = e.urls[first][base];
                                        $http.get("?v="+version+"&url="+encodeURIComponent(url)).success(function(data){
                                            e.buy = data.ask;
                                            e.sell = data.bid;
                                            e.include = true;
                                        });
                                        break;
                                    case "krkn":
                                        var url = e.urls[first][base];
                                        $http.get("?v="+version+"&url="+encodeURIComponent(url)).success(function(data){
                                            e.buy = data.result["X"+first.toUpperCase()+"Z"+base.toUpperCase()].a[0];
                                            e.sell = data.result["X"+first.toUpperCase()+"Z"+base.toUpperCase()].b[0];
                                            e.include = true;
                                        });
                                        break;
                                    case "cryptotrade":
                                        var url = e.urls[first][base];
                                        $http.get("?v="+version+"&url="+encodeURIComponent(url)).success(function(data){
                                            e.buy = data.data.min_ask;
                                            e.sell = data.data.max_bid;
                                            e.include = true;
                                        });
                                        break;
                                    case "vircurex":
                                        var url = e.urls[first][base];
                                        $http.get("?v="+version+"&url="+encodeURIComponent(url)).success(function(data){
                                            e.buy = data.lowest_ask;
                                            e.sell = data.highest_bid;
                                            e.include = true;
                                        });
                                        break;
                                    case "campbx":
                                        var url = e.urls[first][base];
                                        $http.get("?v="+version+"&url="+encodeURIComponent(url)).success(function(data){
                                            e.buy = data['Best Ask'];
                                            e.sell = data['Best Bid'];
                                            e.include = true;
                                        });
                                        break;
                                    case "cbase":
                                        var url = e.urls[first][base];
                                        $http.get("?v="+version+"&url="+encodeURIComponent(url+'buy')).success(function(data){
                                            e.buy = data.total.amount;
                                            $http.get("?v="+version+"&url="+encodeURIComponent(url+'sell')).success(function(data){
                                                e.sell = data.total.amount;
                                                e.include = true;
                                            });
                                        });
                                        break;
                                    case "btcchina":
                                        if(base == "usd")
                                        {
                                            $http.get("?v="+version+"&url="+encodeURIComponent("http://rate-exchange.appspot.com/currency?from=CNY&to=USD")).success(function(data){
                                                exchangeMultiplier = data.rate;
                                                var url = e.urls[first]['cny'];
                                                $http.get("?v="+version+"&url="+encodeURIComponent(url)).success(function(data){
                                                    e.buy = exchangeMultiplier*data.ticker.sell;
                                                    e.sell = exchangeMultiplier*data.ticker.buy;
                                                    e.include = true;
                                                });
                                            }); 
                                        } else {
                                            var url = e.urls[first]['cny'];
                                            $http.get("?v="+version+"&url="+encodeURIComponent(url)).success(function(data){
                                                e.buy = data.ticker.sell;
                                                e.sell = data.ticker.buy;
                                                e.include = true;
                                            });
                                        }
                                        break;
                                    case "okcoin":
                                        if(base == "usd")
                                        {
                                            $http.get("?v="+version+"&url="+encodeURIComponent("http://rate-exchange.appspot.com/currency?from=CNY&to=USD")).success(function(data){
                                                exchangeMultiplier = data.rate;
                                                var url = e.urls[first]['cny'];
                                                $http.get("?v="+version+"&url="+encodeURIComponent(url)).success(function(data){
                                                    e.buy = exchangeMultiplier*data.ticker.sell;
                                                    e.sell = exchangeMultiplier*data.ticker.buy;
                                                    e.include = true;
                                                });
                                            }); 
                                        } else {
                                            var url = e.urls[first]['cny'];
                                            $http.get("?v="+version+"&url="+encodeURIComponent(url)).success(function(data){
                                                e.buy = data.ticker.sell;
                                                e.sell = data.ticker.buy;
                                                e.include = true;
                                            });
                                        }
                                        break;

                                }
                            },10000);
                            
                            
                        }
                    }

                };
                getRates(id,$scope.exchanges,$scope.first,$scope.base);
            };

        }
              

    // $('#first img').click(function() {
    //     var chosen = $(this);
    //     $('#first img').removeClass('checked');
    //     chosen.addClass('checked');
    //     $('#first #sel1').text(chosen.id).fadeIn(100);
    //     $('#baseRow').show();
    //   }
    // ); 
    // $('#base img').click(function() {
    //     var chosen = $(this);
    //     $('#base img').removeClass('checked');
    //     chosen.addClass('checked');
    //     $('#base #sel1').text(chosen.id).fadeIn(100);
    //     $('#exchangeRow').show();
    //   }
    // ); 

    // function getRates() {
