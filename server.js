var http = require('http');
var https = require('https');
var url = require('url');
var static = require('node-static');

http.createServer(function (request, response) {
	var parsedUrl = url.parse(request.url, true); // true to get query as object
	var query = parsedUrl.query;
	
	if(query.url) {
		console.log('url is ' + query.url);
		function sendJSON(json) {
			response.writeHead(200, {'Content-Type': 'text/plain','Access-Control-Allow-Origin' : '*'});
			response.end(json);
		}
		var urlParts = query.url.split('/');
		path = "/"+urlParts[3]+"/"+ ((urlParts[4])? urlParts[4] + "/" :"") + ((urlParts[5])? urlParts[5] + "/":"")+ ((urlParts[6])? urlParts[6] :"");
		if(urlParts[2] != "www.bitstamp.net" && path.slice(-1) == "/")
		{
			path = path.substr(0,path.length-1);
		}
		var options = {
			rejectUnauthorized: false,
			hostname: urlParts[2],
			path:path,
			method: 'GET'
		};
		console.log(JSON.stringify(options));
		if(query.url.indexOf('https') != -1) {
			var req = https.get(options, function(res) {
				var json = "";
				res.setEncoding('utf8');
				res.on('data', function (chunk) {
					json += chunk;
					console.log(chunk);
					console.log("https data loaded");
				});
				res.on('end', function () {
					sendJSON(json);
				});
			});

			req.on('error', function(e) {
				console.log('problem with request: ' + e.message);
			});
		} else {
			var req = http.get(options, function(res) {
				var json = "";
				res.setEncoding('utf8');
				res.on('data', function (chunk) {
					json += chunk;
					console.log(chunk);
					console.log("http data loaded");
				});
				res.on('end', function () {
					sendJSON(json);
				});
			});

			req.on('error', function(e) {
			console.log('problem with request: ' + e.message);
			});
	}} else {
		var file = new static.Server();
			file.serve(request,response);
	}
	
	
}).listen(80);

console.log('Server running at http://127.0.0.1:8124/');